package com.example.lecture10

import android.os.Parcel
import android.os.Parcelable

class Mymodel(val FirstName:String?,val LastName:String?,val Email:String?,val DateofBirth:String?,val Gender:String?):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(FirstName)
        parcel.writeString(LastName)
        parcel.writeString(Email)
        parcel.writeString(DateofBirth)
        parcel.writeString(Gender)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Mymodel> {
        override fun createFromParcel(parcel: Parcel): Mymodel {
            return Mymodel(parcel)
        }

        override fun newArray(size: Int): Array<Mymodel?> {
            return arrayOfNulls(size)
        }
    }
}