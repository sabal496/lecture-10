package com.example.lecture10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashScreen : AppCompatActivity() {

    private lateinit var handler:Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        init()
    }

    private fun init(){
        handler=Handler()
    }

    override fun onStart() {
        super.onStart()
        handler.postDelayed(runnable,2000)
    }
    private  val runnable= Runnable { mainactivity() }

   private fun mainactivity(){

       val intent=Intent(this,MainActivity::class.java)
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
       startActivity(intent)

   }
}
