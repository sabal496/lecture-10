package com.example.lecture10

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
        var REQUEST_CODE=1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init()
    {

        changebtn.setOnClickListener {
            var intent=Intent(this,changeActivity::class.java)
            startActivityForResult(intent,REQUEST_CODE)

       }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
       if(requestCode==REQUEST_CODE && resultCode==Activity.RESULT_OK){
           changedata(data)
          // val name=data?.extras?.getString("data").toString()
          //Toast.makeText(this,name,Toast.LENGTH_SHORT).show()
       }

        super.onActivityResult(requestCode, resultCode, data)
    }

    fun changedata(data: Intent?){
        val model= data!!.extras!!.getParcelable<Mymodel>("data") as Mymodel
        FirstName.text = model.FirstName
        LastName.text=model.LastName
        DateOfBirth.text=model.LastName
        Email.text=model.Email
        Gender.text=model.Gender

    }





}
