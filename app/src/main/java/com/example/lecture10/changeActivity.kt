package com.example.lecture10

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_change.*

class changeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change)
        init()
    }

    private  fun init(){

        savebtn.setOnClickListener {
            senddatta()
        }

    }

    fun senddatta()
    {
        val intent= intent
        val firstname= FirstName.text.toString()
        val lastname=LastName.text.toString()
        val email=Email.text.toString()
        val gender=Gender.text.toString()
        val dateofbirth=DateOfBirth.text.toString()
        if(check()){
            val data=Mymodel(FirstName =firstname,LastName =lastname,Email = email,Gender = gender,DateofBirth = dateofbirth )
            intent.putExtra("data",data)
            setResult(Activity.RESULT_OK,intent)
            finish()

        }
        else
            Toast.makeText(this,"please enter all values",Toast.LENGTH_SHORT).show()

    }

    private fun check():Boolean
    {
        if(FirstName.text.toString().isNullOrEmpty() || LastName.text.toString().isNullOrEmpty()||Email.text.toString().isNullOrEmpty()||Gender.text.toString().isNullOrEmpty()||DateOfBirth.text.isNullOrEmpty())
        return false

        return true
    }

}
